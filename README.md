# Index: 
## Bedtime Scripts: 
I wrote these script to close down my kids various computer at 10 PM on school nights.

![Screenshot](https://img.shields.io/badge/Platform-Linux-darkgreen) ![Screenshot](https://img.shields.io/badge/Language-Bash-blue) https://github.com/ciwen3/Public/tree/master/Bash/bedtime

![Screenshot](https://img.shields.io/badge/Platform-Windows-darkgreen) ![Screenshot](https://img.shields.io/badge/Language-Powershell-blue) https://github.com/ciwen3/Public/blob/master/Powershell/bedtime


## Bug Hunting:
### KQL 
![Screenshot](https://img.shields.io/badge/Platform-Windows-darkgreen) ![Screenshot](https://img.shields.io/badge/Language-Kusto--Query--Language-blue) 
  - https://github.com/ciwen3/Public/blob/master/KQL/README.md
  - https://github.com/ciwen3/Public/blob/master/KQL/WorkBooks.md

### Find Unquoted Path Vulnerabilties
Created to automate the process of finding unquoted path calls that could allow a  attacker to run malicious code for persistance or to elevate priviledges.

![Screenshot](https://img.shields.io/badge/Platform-Windows-darkgreen) ![Screenshot](https://img.shields.io/badge/Language-C++-blue) https://github.com/ciwen3/Public/blob/master/C%2B%2B/Unquoted-Path-Vuln


## Crypto:
### Steganography
Created to hide ascii characters in pictures for retrieval by the same tool. Doesn't actually encyprt data, just encodes (hides) it in the pictures. 

![Screenshot](https://img.shields.io/badge/Platform-Universal-darkgreen) ![Screenshot](https://img.shields.io/badge/Language-Python3-blue)  https://github.com/ciwen3/Public/tree/master/python/steganography
### Ceasar Cypher 
Implementation of a very old encryption cypher. It only works on letters of the alphabet. This means numbers and punctuation will not be encrypted. this is easy to add by changing the alphabet variables. 

![Screenshot](https://img.shields.io/badge/Platform-Universal-darkgreen) ![Screenshot](https://img.shields.io/badge/Language-Python2-blue) https://github.com/ciwen3/Public/blob/master/python/ceasar/ceasar2.py
### CyptoPal Challenges
![Screenshot](https://img.shields.io/badge/Platform-Universal-darkgreen) ![Screenshot](https://img.shields.io/badge/Language-PythonMixed-blue) https://github.com/ciwen3/Public/tree/master/python/CryptoPals

## Education:
### Documents (Certifications, Degree & GED)
https://github.com/ciwen3/Public/tree/master/Education/Documents

### Class Projects
![Screenshot](https://img.shields.io/badge/Platform-Windows-darkgreen) ![Screenshot](https://img.shields.io/badge/Language-VisualBasic-blue) https://github.com/ciwen3/Public/tree/master/Education/VB%20Scripts
https://github.com/ciwen3/Public/tree/master/Education/Business%20Class%20Project
https://github.com/ciwen3/Public/tree/master/Education/Cisco%20Capstone%20Project
https://github.com/ciwen3/Public/tree/master/Education/Speech%20275

## Hacking:
### OSCP Study & Attack repo
A collection of information I have gather to take the OSCP exam. 

https://github.com/ciwen3/OSCP

### Chickenman-Alt Instructions
Game to practice WiFi hacking. 

https://github.com/ciwen3/Public/blob/master/ChickenmanAltInstructions/README.md

### ChromeBook
One of several Chromebook models I have turned into Linux Laptops. 

https://github.com/ciwen3/Public/blob/master/Chromebook/README.md

### Rubber Ducky / Attiny85 Scripts 
Scripts to automatically attack a system by plugging in a usb devcice that mimics a keyboard and issues commands at lightning speed. 

https://github.com/ciwen3/Public/tree/master/RubberDucky


## FUN:
## Prime Numbers: 
I created these programs to test my theory that all prime numbers will have a remainder of 1 or 5 when divided by 6 (except 2 or 3, which are used derive the value of 6 in the equation).

![Screenshot](https://img.shields.io/badge/Platform-Linux-darkgreen) ![Screenshot](https://img.shields.io/badge/Language-Bash-blue) https://github.com/ciwen3/Public/tree/master/python/Prime-Counter

### DIY Arcade
Full size arcade and cabinet that I made for a friend. 

https://github.com/ciwen3/Public/blob/master/DIY%20Arcade/README.md

### Job Search Script
https://github.com/ciwen3/Public/blob/master/Bash/job-search.sh

### Art Projects I have participated in
https://github.com/ciwen3/Public/blob/master/Art.md

## Other
https://www.youtube.com/watch?v=dQw4w9WgXcQ?autoplay=1



# All of my Public projects, opinions and advice are offered “as-is”, without warranty, and disclaiming liability for damages resulting from using any of my software or taking any of my advice.
