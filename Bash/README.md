# Bedtime Bash Script
![Screenshot](https://img.shields.io/badge/Language-Bash-blue)
![Screenshot](https://img.shields.io/badge/Platform-Linux-brightgreen)

https://github.com/ciwen3/Public/tree/master/Bash/bedtime

used to power down my kids Linux computers at a set time. the files can be installed anywhere so long as your kids don't have sudo access and you use the chattr command to make the scripts immutable.

# Burn It All
![Screenshot](https://img.shields.io/badge/Language-Bash-blue)
![Screenshot](https://img.shields.io/badge/Platform-Linux-brightgreen)

https://github.com/ciwen3/Public/blob/master/Bash/BurnItAll.sh

I helped a company with an infection, we figured out it was known as XCSSET. In my research of the Malware we found and the information online I put together a script to systematically download each version of the Malware from each server they have check its SHA256 hash against a list of already downloaded malware and if it is unique (to me) upload it to virus total. 

# Job Search
![Screenshot](https://img.shields.io/badge/Language-Bash-blue)
![Screenshot](https://img.shields.io/badge/Platform-Linux-brightgreen)

https://github.com/ciwen3/Public/blob/master/Bash/job-search.sh

A fake script I wrote as a job when looking for jobs. 
